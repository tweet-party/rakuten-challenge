## link to challenge:
https://challengedata.ens.fr/participants/challenges/59/

## dataset path on the cluster:
/opt/Datasets/RakutenMultiModalColourExtraction/

## pytorch link to create its own dataset class from extending the torch.utils.data.Dataset class:
https://pytorch.org/tutorials/beginner/data_loading_tutorial.html

## pytorch link to densenet121:
https://pytorch.org/vision/0.8/models.html#id16

## plot japanese font:
https://albertauyeung.github.io/2020/03/15/matplotlib-cjk-fonts.html

## skim age installation for python:
python -m pip install -U scikit-image

## unicode in python:
https://docs.python.org/3/howto/unicode.html

## on the cluster shell, Python3 interpreter in a ASCII locale, to force a UTF-8 locale: 
PYTHONIOENCODING=UTF-8 python3 dataset.py 

## to handle corrupted images:
https://discuss.pytorch.org/t/questions-about-dataloader-and-dataset/806/4
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

## for text transformations explore the already available torchtext.utils functions: 
https://pytorch.org/text/_modules/torchtext/utils.html

## for Japanese specific word segmentation:
https://investigate.ai/text-analysis/how-to-make-scikit-learn-natural-language-processing-work-with-japanese-chinese/

## for LSTM with different sequence length:
https://discuss.pytorch.org/t/rnn-with-different-sequence-lengths/84922

https://stackoverflow.com/questions/49466894/how-to-correctly-give-inputs-to-embedding-lstm-and-linear-layers-in-pytorch/49473068#49473068

