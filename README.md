# Rakuten Project: Predicting color tags simultaneously from image and text.

The _Rakuten Project_ is part of the 2021 ENS datasience challenge. It aims at building an algorithm capable of extracting color tags
from a product description composed of an image, a title and a description.

More information about the Rakuten challenge [here](https://challengedata.ens.fr/challenges/59).

# Project Structure:

* _dataset.py_: implements data pre-processing and transformations to build a custom _Rakuten Dataset_ . 

* _models.py_: defines neural network models.

* _train.py_: is the main module, for training the models.

* _utils.py_: gathers functions used during training and inference.

# It's simple to run!

To run the _Rakuten Project_ you only need the following command:

```Shell
python3 train.py --model="Conv121Net" --image_size=100
```
-> argument *image_size* directly influences the number of model parameters.

# Available models are:

**Using images only as inputs:**

* _Conv121Net_: in this model, pre-trainned DenseNet121 is used to perform image feature extraction.

* _MyConvNet_: in this model, a custom convolutionnal network is used for image feature extraction.

**Using images, titles and descriptions as inputs:**

* _ConvRnnNet_: in this model, a custom convolutionnal network is used for image feature extraction and a custom RNN is used for title and description sequence analysis.

* _Conv121RnnNet_: in this model, pre-trainned DenseNet121 is used to perform image feature extraction and a custom RNN is used for title and description sequence analysis.

* _ConvCountVectorizer_: in this model, a custom convolutionnal network is used for image feature extraction and CountVectorizer is used for title and description analysis.

# Video and Presentation :
Our Video and presentation are downloadable via this google drive link [here](https://drive.google.com/drive/folders/1g-MvBh6wrls62-nGGsKMQ9LbNl3pQatD?usp=sharing). 
