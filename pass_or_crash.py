import pandas as pd
import ast
import torch
import torch.nn as nn
'''import torchtext
from torchtext.data import get_tokenizer'''
from torch.utils.data.dataloader import default_collate
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np
###############################
## string to list conversion ##
###############################

"""
dataset_dir="/Users/Paul/Desktop/challenge_ens/rakuten-challenge/data"

y_df = pd.read_csv(dataset_dir + '/y_train.csv', encoding='utf-8').iloc[:, 1:] # encoding to utf-8 from the start
print("y_df shape is: {}".format(y_df.shape))

k=2
string = y_df.iloc[k, 0]
print(type(string))
print(string)

liste = ast.literal_eval(string)
print(type(liste))
print(liste[0])
"""

################################
## list alphabetic reordering ##
################################

"""
classes = ['Silver', 'Grey', 'Black', 'Brown', 'White', 'Beige', 'Multiple Colors', 'Gold', 'Yellow', 
            'Orange', 'Pink', 'Red', 'Purple', 'Green', 'Blue', 'Navy', 'Khaki', 'Burgundy', 'Transparent']

print(sorted(classes))
# ['Beige', 'Black', 'Blue', 'Brown', 'Burgundy', 'Gold', 'Green', 'Grey', 'Khaki', 'Multiple Colors', 
# 'Navy', 'Orange', 'Pink', 'Purple', 'Red', 'Silver', 'Transparent', 'White', 'Yellow']

print(len(classes))
"""

#############################################
## get_tokenizer from torchtext.data.utils ##       https://pytorch.org/text/stable/data_utils.html#get-tokenizer
#############################################

"""
tokenizer = get_tokenizer("basic_english")
tokens = tokenizer("You can now install TorchText using pip!")
print(tokens)

tokenizer = get_tokenizer(None, language='jp')
tokens = tokenizer("【 UES（ウエス） 】 67LW オリジナル吊り編み鹿の子ポロシャツ [ Tシャツ ][ アメカジ ] [ メンズ ] [ 送料・代引き手数料無料 ]")
print(tokens)
"""

#######################################
## text standardization and cleaning ##               http://sdi.metz.centralesupelec.fr/spip.php?article33
#######################################

import re
import string

txt = "【 UES（ウエス） 】 67LW オリジナル吊り編み鹿の子ポロシャツ [ Tシャツ ][ アメカジ ] [ メンズ ] [ 送料・代引き手数料無料 ]"

"""
# To lower case
print("To lower case")
txt = txt.lower()
print(txt,"\n")

# Remove links
print("Removing the links")
url_regexp = "https?://[\w\/\.]+"
txt = re.sub(url_regexp, ' ', txt)
print(txt,"\n")

# Remove the HTML tags <.../>
print("Removing the HTML tags")
txt = re.sub('<[ a-zA-Z]*/?>', ' ', txt)
print(txt,"\n")

# Remove punctuation
print("Removing the punctuation")
txt = re.sub('[%s]' % re.escape(string.punctuation), ' ', txt)
print(txt,"\n")

# Remove selected special characters "【 】・": 
print("Removing selected special characters '【 】・' ")
txt = re.sub('[%s]' % re.escape(' 【 】（）・'), ' ', txt)
print(txt,"\n")

# Remove linebreaks
print("Removing the linebreaks")
txt = re.sub('\n', ' ', txt)
print(txt,"\n")

# Remove words containing numbers
print("Removing words containing numbers")
txt = re.sub('\w*\d\w*', ' ', txt)
print(txt,"\n")

# Remove duplicated spaces
print("Removing duplicated spaces")
txt = re.sub(' {2,}'," ", txt)
print(txt,"\n")
"""

# final output: "ues ウエス オリジナル吊り編み鹿の子ポロシャツ tシャツ アメカジ メンズ 送料 代引き手数料無料"

# il faut peut être encore éliminer le "ues" et le "t" ?

# --> notice that in Japanese, stemming does not apply <--
# --> How to correctly group Japanese characters ? <--

####################################
## feature extraction in Japanese ##               https://investigate.ai/text-analysis/how-to-make-scikit-learn-natural-language-processing-work-with-japanese-chinese/
####################################

"""
import nagisa

text = 'ues ウエス オリジナル吊り編み鹿の子ポロシャツ tシャツ アメカジ メンズ 送料 代引き手数料無料'
doc = nagisa.tagging(text)
print("nagissa doc is: {}".format(doc))
print("nagissa doc.words is: {}".format(doc.words))

# Takes in a document, returns the list of words
def tokenize_jp(doc):
    doc = nagisa.tagging(doc)
    return doc.words

L = ['ues ウエス オリジナル吊り編み鹿の子ポロシャツ tシャツ アメカジ メンズ 送料 代引き手数料無料']

vectorizer = CountVectorizer(tokenizer=tokenize_jp) # use the customed tokenizer
vectorized_text = vectorizer.fit_transform(L)
vocab = vectorizer.get_feature_names()

print(vocab)
print(vectorized_text.toarray())
"""

###############################################
## naïve vectorizer with customed vocabulary ##
###############################################

"""
L = ['ues ウエス オリジナル吊り編み鹿の子ポロシャツ tシャツ アメカジ メンズ 送料 代引き手数料無料', '手手手']
vocab = set( [charac for charac in L[0]] )
vocab_size = len(vocab)
character_to_ix = {charac: i for i, charac in enumerate(vocab)}

print("the vocabulary is:{}".format(vocab))
print("character_to_ix is:{}".format(character_to_ix))

def mytokenizer(text):
    return [charac for charac in text]

vectorizer = CountVectorizer(vocabulary = character_to_ix, tokenizer = mytokenizer)
vectorized_text = vectorizer.transform(L)

print(vectorized_text.toarray())
"""

#########################################
## label encoding and words embeddings ##      https://pytorch.org/tutorials/beginner/nlp/word_embeddings_tutorial.html
#########################################

"""
embedding_dim = 20

embeds = nn.Embedding(vocab_size, embedding_dim)  # vocab_size words in vocab, 20 dimensional embeddings
lookup_tensor = torch.tensor([character_to_ix["鹿"]], dtype=torch.long)
鹿_embed = embeds(lookup_tensor)

print("the embedding of character 鹿 is: {}".format(鹿_embed))

# turn the words into integer indices and wrap them in tensors
characters_idxs = torch.tensor([character_to_ix[charac] for charac in L[0]], dtype=torch.long) # label encoding
"""

###########################
## test on torch tensors ##
###########################

"""
A = [1,2,3]
B = [4,6,9]
for i, (a, b) in enumerate(zip(A,B)):
    print(a,b)

T = torch.tensor([1,2,3,4,5])
print(tuple(T.shape))
T = torch.cat([T[0:i], T[i+1:]])
print(T)
"""

#########################################
## test image resizing for the dataset ##
#########################################

'''def __getitem__(self, idx):

    if torch.is_tensor(idx):
        idx = idx.tolist()

    img_name = os.path.join(self.dataset_dir + "/images/", self.rakuten_frame.iloc[idx, 0])
    image = io.imread(img_name) # ---> one image is corrupted and causes io.imread to fail <---

    #Fixing Problem of not well resized Images
    if len(image.shape) != 3 or image.shape[0] != 300 or image.shape[1] != 300:
        sample = None

    elif image.shape[2]==4 :
        #print(image.shape)
        image = rgba2rgb(image)
        title = self.rakuten_frame.iloc[idx, 1]
        description = self.rakuten_frame.iloc[idx, 2]
        tags = ast.literal_eval(self.rakuten_frame.iloc[idx, 3]) # a list of tags
        sample = {'image': image, 'title': title, 'description': description, 'tags': tags}

    else : 
        title = self.rakuten_frame.iloc[idx, 1]
        description = self.rakuten_frame.iloc[idx, 2]
        tags = ast.literal_eval(self.rakuten_frame.iloc[idx, 3]) # a list of tags

        sample = {'image': image, 'title': title, 'description': description, 'tags': tags}

    if self.transform:
        sample = self.transform(sample)
    return sample'''

##################################################
## test on custom collate_fn for the Dataloader ##
##################################################

'''def costum_collate_fn(batch):
    #new_batch = []
    for i, item in enumerate(batch):
        if tuple(item['image'].shape) != (3, 300, 300):
            print('detected with {}'.format(tuple(item['image'].shape)))
            batch = batch[:i]+batch[i+1:]
    return default_collate(batch)

batch_test = [{'image' : np.zeros((3, 300, 300)), 'tags':'aymane'},
        {'image' : np.zeros((2, 300, 300)), 'tags':'achraf'}]

print(costum_collate_fn(batch_test))

a = np.zeros((4, 300, 300))
a_reshaped = np.delete(a, 3, axis=0)
print(a_reshaped.shape)'''

########################
## test on torch LSTM ##
########################

"""
classes = ['Beige', 'Black', 'Blue', 'Brown', 'Burgundy', 'Gold', 'Green', 'Grey', 'Khaki', 'Multiple Colors', 
            'Navy', 'Orange', 'Pink', 'Purple', 'Red', 'Silver', 'Transparent', 'White', 'Yellow']

pred = [1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0]

print([classes[i] for i in range(len(classes)) if pred[i]==1])

import torch.nn as nn
rnn = nn.LSTM(100, 256, 5)
input = torch.randn(1, 3766, 100)
output, _ = rnn(input)
print(output.size())


intt1 = torch.randn(4, 65536)
#intt1 = nn.ConstantPad1d((-1, 1928192 - 65536), 0)(intt1)
intt2 = torch.randn(4, 1928192)
intt3 = torch.randn(4, 1928192)
out = torch.cat([intt1, intt2, intt3],1)
print(out.size())
"""

###########################
## test on torch tensors ##
###########################

batch_size = 32
max_length_titles = 50

padded_titles = np.zeros((batch_size, max_length_titles))

padded_titles[0, 0:max_length_titles-20] = 5

print(padded_titles)


#get weights
'''labels_tensors = torch.stack([sample['tags'] for sample in trainval_dataset])
nb_elt_classes = labels_tensors.sum(axis=0)
percentages_classes = nb_elt_classes/len(labels_tensors)
print(percentages_classes)'''

#extract_save_features function
'''def extract_save_features(loader: torch.utils.data.DataLoader,
                        model: torch.nn.Module,
                        device: torch.device,
                        filename: str):
    all_features = []
    all_targets = defaultdict(list)

    with torch.no_grad():
        model.eval()
        for (inputs, targets) in tqdm(loader):

            inputs = inputs.to(device=device)

            # Compute the forward propagation through the body
            # just to extract the features
            all_features.append(model(inputs))

            for k, v in targets.items():
                all_targets[k].append(v)

        for k, v in all_targets.items():
            all_targets[k] = torch.squeeze(torch.cat(v, 0))
        all_features = torch.squeeze(torch.cat(all_features , 0))
    print(all_features.shape)
    print("The features that are saved are {} features maps of size {} x {}, with {} channels".format(all_features.shape[0], all_features.shape[2], all_features.shape[3], all_features.shape[1]))
    for k, v in all_targets.items():
        print("The entry {} have shape {}".format(k, v.shape))

    torch.save(dict([("features", all_features)] + list(all_targets.items())), filename)'''


a = torch.randn(2,19)
print(a)
out = torch.sigmoid(a)
print(out)
out = (torch.sigmoid(a)>0.5).int()
print(out)