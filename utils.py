# -*- coding: utf-8 -*-

##########################
##   Import Libraries   ##
##########################

from collections import defaultdict
from tqdm import tqdm
import numpy as np, os, sys
import pandas as pd
from typing import Tuple

import re
import string

import torch
import torch.nn as nn
from torch.utils.data.dataloader import default_collate
from torch.utils.data import TensorDataset, DataLoader
from torch.utils.tensorboard import SummaryWriter
from torch.nn.modules.module import _addindent
from sklearn.metrics import f1_score
from sklearn.metrics import classification_report
from sklearn.feature_extraction.text import CountVectorizer

##########################
## DataLoader Functions ##
##########################

def vectorizer_collate_fonction(image_size):

    def vectorizer_collate_fn(batch):
        for item in batch:
            if tuple(item['image'].shape) != (3, image_size, image_size):
                item['image'] = np.delete(item['image'], 3, axis=0)
        return default_collate(batch)
    
    return vectorizer_collate_fn

def custom_collate_fonction(image_size):

    def custom_collate_fn(batch):

        batch_size = len(batch)

        # 1/ Dealing with Images
        for item in batch:
            if tuple(item['image'].shape) != (3, image_size, image_size):
                item['image'] = np.delete(item['image'], 3, axis=0)
        batch_images = [item['image'] for item in batch]
        batch_images = torch.stack(batch_images)

        # 2/ Dealing with Titles
        batch_titles = [item['title'] for item in batch]
        lenghts_titles = [(item['title'].shape)[0] for item in batch]
        max_length_titles = max(lenghts_titles)
        padded_titles = np.zeros((batch_size, max_length_titles))

        for i, l_title in enumerate(lenghts_titles):
            # zero padding at the end of the sequence:
            # padded_titles[i, 0:l_title] = batch_titles[i][0:l_title]

            # constant padding at the beginning of the sequence:
            padded_titles[i, 0:max_length_titles-l_title] = batch_titles[i][0]
            if 1 <= l_title:
                padded_titles[i, max_length_titles-l_title+1:max_length_titles] = batch_titles[i][1:l_title]
        
        batch_titles = torch.tensor(padded_titles)
        #print("size of titles : {}".format(batch_titles.size()))

        # 3/ Dealing with Descriptions
        batch_descriptions = [item['description'] for item in batch]
        lenghts_descriptions = [(item['description'].shape)[0] for item in batch]
        max_length_descriptions = max(lenghts_descriptions)
        padded_descriptions = np.zeros((batch_size, max_length_descriptions))

        for i, l_description in enumerate(lenghts_descriptions):
            # zero padding at the end of the sequence:
            # padded_descriptions[i, 0:l_description] = batch_descriptions[i][0:l_description]

            # constant padding at the beginning of the sequence:
            padded_descriptions[i, 0:max_length_descriptions-l_description] = batch_descriptions[i][0]
            if 1 <= l_description:
                padded_descriptions[i, max_length_descriptions-l_description+1:max_length_descriptions] = batch_descriptions[i][1:l_description]
        
        batch_descriptions = torch.tensor(padded_descriptions)
        #print("size of descriptions : {}".format(batch_descriptions.size()))

        #Dealing with Tags
        if 'tags' in batch[0].keys():
            batch_tags = [item['tags'] for item in batch]
            batch_tags = torch.stack(batch_tags)
            dict_batch = {'image': batch_images,'title':batch_titles,'description':batch_descriptions,'tags':batch_tags}
            return dict_batch        
        else:
            dict_batch = {'image':batch_images,'title':batch_titles,'description':batch_descriptions}
            return dict_batch
        
    return custom_collate_fn

#####################
## Model Functions ##
#####################

def train(model, loader, f_loss, optimizer, device):

    targets = []
    predictions = []
    N, tot_loss, correct = 0, 0.0, 0
    running_TP,running_FP, running_FN = 0, 0, 0

    for batch in tqdm(loader):

        images = batch['image'].float()
        labels = batch['tags'].float()
        titles = batch['title'].long()
        descriptions = batch['description'].long()
        images, titles, descriptions, labels = images.to(device), titles.to(device), descriptions.to(device), labels.to(device)

        enteries = [images, titles, descriptions]

        outputs = model(enteries)
        loss = f_loss(outputs, labels)
        #loss = (loss * weights).mean() 

        # Backprop and perform Adam optimisation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        # Accumulate the number of processed samples
        N += images.shape[0]

        # For the total loss
        tot_loss += images.shape[0] * loss.item()

        # Predictions BCELogitsloss()
        predicted = (torch.sigmoid(outputs)>0.5).int()

        # Track the accuracy
        label_size = labels.shape[1]  
        correct += (predicted == labels).sum().item()
        running_TP += (predicted*labels).sum().item()
        running_FP += (predicted*(1-labels)).sum().item()
        running_FN += ((1-predicted)*labels).sum().item()
        running_loss = tot_loss/N
        running_accuracy = correct/(N*label_size)

        #Track the F1 Score
        predictions.append(predicted.cpu().numpy())
        targets.append(labels.cpu().numpy())

    #F1 Score calculation
    predictions = np.concatenate(predictions)
    targets = np.concatenate(targets)

    #F1 Score Handcrafted
    precision = running_TP/ (running_TP+ running_FP)
    recall = running_TP / (running_TP + running_FN)
    #F1_Score = 2*(precision*recall)/(precision+recall)

    #The right way to get F1-weighted Score
    
    report = classification_report(predictions, targets, output_dict=True)
    F1_Score = report['weighted avg']['f1-score']
    #print('f1_training is = {}'.format(F1_Score))
    

    return(running_loss,running_accuracy,F1_Score)


def valid(model, loader, f_loss, device):

    with torch.no_grad():
        
        val_targets = []
        val_predictions = []
        N_val , tot_val_loss, val_correct = 0, 0.0, 0
        running_val_TP,running_val_FP, running_val_FN = 0, 0, 0

        # get validation data from data_loader
        for batch in tqdm(loader):
            
            valid_images = batch['image'].float()
            valid_labels = batch['tags'].float()
            valid_titles = batch['title'].long()
            valid_descriptions = batch['description'].long()
            valid_images, valid_labels = valid_images.to(device), valid_labels.to(device)
            valid_titles, valid_descriptions = valid_titles.to(device), valid_descriptions.to(device)
            
            val_enteries = [valid_images, valid_titles, valid_descriptions]

            outputs = model(val_enteries)
            val_loss = f_loss(outputs, valid_labels)
            #val_loss = (val_loss * weights).mean()

            N_val += valid_images.shape[0]
            tot_val_loss += valid_images.shape[0] * val_loss.item()

            valid_predicted = (torch.sigmoid(outputs)>0.5).int()

            # Track Validation accuracy
            label_size = valid_labels.shape[1]
            val_correct += (valid_predicted == valid_labels).sum().item()
            running_val_TP += (valid_predicted*valid_labels).sum().item()
            running_val_FP += (valid_predicted*(1-valid_labels)).sum().item()
            running_val_FN += ((1-valid_predicted)*valid_labels).sum().item()
            running_loss = tot_val_loss/N_val
            running_accuracy = val_correct/(N_val*label_size)

            #Track the F1 Score
            val_predictions.append(valid_predicted.cpu().numpy())
            val_targets.append(valid_labels.cpu().numpy())
        
        precision = running_val_TP/ (running_val_TP+ running_val_FP)
        recall = running_val_TP / (running_val_TP + running_val_FN)
        F1_Score_val = 2*(precision*recall)/(precision+recall)

        #F1 Score calculation
        val_predictions = np.concatenate(val_predictions)
        val_targets = np.concatenate(val_targets)

        #Classification report
        #print(classification_report(val_predictions, val_targets))

        #The right way to get F1-weighted Score
        
        report = classification_report(val_predictions, val_targets, output_dict=True)
        F1_Score_val = report['weighted avg']['f1-score']
        #print('f1_training is = {}'.format(F1_Score_val))
        

    return(running_loss,running_accuracy,F1_Score_val)


def test(model, loader, f_loss, device, classes):

    with torch.no_grad():
        
        test_predictions = []

        # get validation data from data_loader
        for batch in tqdm(loader):
            
            test_images = batch['image'].float()
            test_images = test_images.to(device)

            test_titles = batch['title'].long()
            test_titles = test_titles.to(device)
            
            test_descriptions = batch['description'].long()
            test_descriptions = test_descriptions.to(device)

            test_enteries = [test_images, test_titles, test_descriptions]
            outputs = model(test_enteries)

            test_predicted = (torch.sigmoid(outputs)>0.5).int()
            test_predictions.append(test_predicted.cpu().numpy())

        #Test predictions
        test_predictions = np.concatenate(test_predictions)
        names_predictions = [[classes[i] for i in range(len(classes)) if pred[i]==1] for pred in test_predictions]

    return names_predictions

###############################
## Save model info functions ##
###############################

def generate_unique_logpath(logdir, raw_run_name):
    i = 0
    while(True):
        run_name = raw_run_name + "_" + str(i)
        log_path = os.path.join(logdir, run_name)
        if not os.path.isdir(log_path):
            return log_path
        i = i + 1

def torch_summarize(model, show_weights=True, show_parameters=True):
    """Summarizes torch model by showing trainable parameters and weights.
        Return a tuple of the summary and the total number of parameters
    """
    tmpstr = model.__class__.__name__ + ' (\n'
    total_params = 0
    for key, module in model._modules.items():
        if type(module) in [
            torch.nn.modules.container.Container,
            torch.nn.modules.container.Sequential
        ]:
            modstr, _ = torch_summarize(module)
        else:
            modstr = module.__repr__()
        modstr = _addindent(modstr, 2)

        params = sum([np.prod(p.size()) for p in module.parameters()])
        total_params += params
        weights = tuple([tuple(p.size()) for p in module.parameters()])

        tmpstr += '  (' + key + '): ' + modstr
        if show_weights:
            tmpstr += ', weights={}'.format(weights)
        if show_parameters:
            tmpstr +=  ', parameters={}'.format(params)
        tmpstr += '\n'

    tmpstr = tmpstr + ')'
    tmpstr += '\n {} learnable parameters'.format(total_params)
    return tmpstr, total_params

def model_summary(model, optimizer, logdir):
    """ Generate and dump the summary of the model.
        model   a pytorch model object
    """
    model_summary, number_of_params = torch_summarize(model)
    # print("Summary:\n {}".format(model_summary))
    print("Total number of parameters: {}".format(number_of_params))

    summary_file = open(logdir + "/summary.txt", 'w')
    summary_text = """
    Executed command
    ===============
    {}
    Dataset
    =======
    Model summary
    =============
    {}
    {} trainable parameters
    Optimizer
    ========
    {}
    """.format(" ".join(sys.argv),
            str(model).replace('\n','\n\t'),
            sum(p.numel() for p in model.parameters() if p.requires_grad),
            str(optimizer).replace('\n', '\n\t'))
    summary_file.write(summary_text)
    summary_file.close()

    return summary_text

class ModelCheckpoint:

    def __init__(self, filepath, model):
        self.min_loss = None
        self.filepath = filepath
        self.model = model 

    def update(self, loss):
        if (self.min_loss is None) or (loss < self.min_loss):
            print("Saving a better model")
            torch.save(self.model, self.filepath)
            self.min_loss = loss

#################################################
## text preprocessing and word_to_ix functions ##
#################################################

def text_preprocessing(txt):

    if isinstance(txt,str):
        # To lower case
        txt = txt.lower()

        # Remove links
        url_regexp = "https?://[\w\/\.]+"
        txt = re.sub(url_regexp, ' ', txt)

        # Remove the HTML tags <.../>
        txt = re.sub('<[ a-zA-Z]*/?>', ' ', txt)

        # Remove punctuation
        txt = re.sub('[%s]' % re.escape(string.punctuation), ' ', txt)

        # Remove selected special characters "【 】・": 
        txt = re.sub('[%s]' % re.escape(' 【 】（）・'), ' ', txt)

        # Remove linebreaks
        txt = re.sub('\n', ' ', txt)

        # Remove words containing numbers
        txt = re.sub('\w*\d\w*', ' ', txt)

        # Remove duplicated spaces
        txt = re.sub(' {2,}'," ", txt)
    
    else:
        #output sequence is always of length > 1:
        txt = " "

    return txt

def token_to_ix(dataset_dir:str, token:str = "word"):
    
    # we build a vocabulary both from the tile and decription of both the training and tests sets:
    vocab_frame = pd.concat([pd.read_csv(dataset_dir + '/X_train.csv', encoding='utf-8').iloc[:, 1:],
                            pd.read_csv(dataset_dir + '/X_test.csv', encoding='utf-8').iloc[:, 1:]
                            ],ignore_index=True)

    text_list = vocab_frame["item_name"].tolist() + vocab_frame["item_caption"].tolist()
    print( "number of text enteries (title and description) is: {}".format(len(text_list)) )

    text_list_processed = [ text_preprocessing(text) for text in text_list ]

    res = []
    for text in text_list_processed:

        if token == "character": # a token corresponds is a Japanese character
        
            for charac in text:
                res += [charac]

        if token == "word": # a token is a Japanese word (group of characters, identified by the nagisa module)
            # --> the nagissa module needs to be installed on the CentralSupelec cluster <--
            """
            import nagisa

            for word in nagisa.taggin(text).words:
                res += [word]
            """

    vocab = set(res)
    token_to_ix = {charac: i for i, charac in enumerate(vocab)}

    print("length of the {} vocabulary is: {}".format(token, len(vocab)))

    return token_to_ix

# vectorizer converts a text to a list of token count, list length is fixed an equal to the vocabulary size: 
def vectorizer(to_ix, token:str = "word"):
    """return a vecorizer based on vocabulary to_ix
        to_ix   a dictionary representing the vocabulary
        token   a str specifying the token mode (either character or word)
    """
    if token == "character":
        def mytokenizer(text):
            return [charac for charac in text]

    if token == "word":
        # --> the nagissa module needs to be installed on the CentralSupelec cluster <--
        """
        def mytokenizer(text):
            text = nagisa.tagging(text)
            return text.words
        """

    vectorizer = CountVectorizer(vocabulary=to_ix,tokenizer = mytokenizer) # use cutom vocab and custom vectorizer

    return vectorizer

# prepare_sequence convert a text to a sequence of int representing tokens, the sequence length depends on the text length 
def prepare_sequence(seq, to_ix, token:str = "word"):
    """returns a list of int (of variating length) representing the sequence
        seq     a str representing either a title or a description
        to_ix   a dictionary representing the vocabulary
        token   a str specifying the token mode (either character or word)
    """

    if token == "character":
        idxs = [to_ix[c] for c in seq] # analysed as a sequence of characters

    if token == "word":
        # --> the nagissa module needs to be installed on the CentralSupelec cluster <--
        """
        idx = [ to_ix[w] for w in nagisa.taggin(seq).words] # analysed as a sequence of words (as defined by the nagissa module)
        """

    return idxs