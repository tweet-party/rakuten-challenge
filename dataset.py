# -*- coding: utf-8 -*-
#export PYTHONIOENCODING=utf-8

##########################
##   Import Libraries   ##
##########################

from __future__ import print_function, division, unicode_literals
import os, sys, ast
from tqdm import tqdm
import cv2
import re
import string
import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
#plt.ion()   # interactive mode

import matplotlib.font_manager as fm
font_file = "./Varieties/NotoSansCJKtc-Regular.otf"
fprop = fm.FontProperties(fname=font_file)

from skimage import io
from skimage import transform as skimage_transform # to avoid conflict
from skimage.color import rgba2rgb
from PIL import ImageFile
from PIL import Image
ImageFile.LOAD_TRUNCATED_IMAGES = True

import torch
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils

import warnings # Ignore warnings
#warnings.filterwarnings("ignore")

import utils as myutils
import dataset_test

######################################
## Rakuten Dataset class definition ##
######################################

class RakutenDataset(Dataset):
    """Rakuten dataset."""

    def __init__(self, dataset_dir, transform=None):
        """
        Args:
            dataset_dir (string): Path to the dataset directory.
            transform (callable, optional): Optional transform to be applied on a sample.
        """
        self.dataset_dir = dataset_dir
        self.rakuten_frame = pd.merge(pd.read_csv(self.dataset_dir + '/X_train.csv', encoding='utf-8').iloc[:, 1:], 
                pd.read_csv(self.dataset_dir + '/y_train.csv', encoding='utf-8').iloc[:, 1:], 
                left_index=True, right_index=True)
        self.transform = transform

    def __len__(self):
        return len(self.rakuten_frame)

    def __getitem__(self, idx):

        if torch.is_tensor(idx):
            idx = idx.tolist()

        img_name = os.path.join(self.dataset_dir + "/images/", self.rakuten_frame.iloc[idx, 0])
        image = io.imread(img_name) # ---> one image is corrupted and causes io.imread to fail <---

        title = self.rakuten_frame.iloc[idx, 1]
        description = self.rakuten_frame.iloc[idx, 2]
        tags = ast.literal_eval(self.rakuten_frame.iloc[idx, 3]) # a list of tags

        sample = {'image': image, 'title': title, 'description': description, 'tags': tags}

        if self.transform:
            sample = self.transform(sample)

        return sample

#################################
## Transforms class definition ##
#################################

class Rescale(object):
    """Rescales the image in a sample to a given size.

    Args:
        output_size (tuple or int): Desired output size. If tuple, output is
            matched to output_size. If int, output is matched to (int,int) size.
    """
    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple)) #checks that arg output_size is either int or tuple
        self.output_size = output_size

    def __call__(self, sample):

        image = sample['image']

        if isinstance(self.output_size, int):
            # if arg output_size an int:
            new_h, new_w = self.output_size, self.output_size
        else:
            # if arg output_size a tuple:
            new_h, new_w = self.output_size

        img = skimage_transform.resize(image, (new_h, new_w))
        sample['image'] = img

        return sample


class VocabEncode(object):
    """Encodes tile and descriptions according to vocabulary to_idx or a vectorizer"""

    def __init__(self, to_ix, token = "word", vectorizer=None):
        assert isinstance(to_ix, dict) # checks that to_ix is a dictionary
        self.to_ix = to_ix
        self.vectorizer = vectorizer
        self.token = token

    def __call__(self, sample):

        if self.vectorizer:
            # CountVectorizer: usefull to directly perform the multi outputs classification
            sample['title'] = self.vectorizer.transform( [myutils.text_preprocessing(sample['title'])] ).toarray() [0]
            sample['description'] = self.vectorizer.transform( [myutils.text_preprocessing(sample['description'])] ).toarray() [0]

        else:
            # Label encoding: usefull for RNN input
            sample['title'] = myutils.prepare_sequence(myutils.text_preprocessing(sample['title']), self.to_ix, self.token)
            sample['description'] = myutils.prepare_sequence(myutils.text_preprocessing(sample['description']), self.to_ix, self.token)

        return sample

class TargetEncode(object):
    """One hot encodes target according to arg classes"""

    def __init__(self, classes):
        assert isinstance(classes, list) # checks that classes is a list
        self.classes = classes

    def __call__(self, sample):
        
        tags = sample['tags']
        # idea 1, label encoding of the targets:
        # sample['tags'] = [self.classes.index(tag) for tag in tags] # generates list of various length
        
        # idea 2, one hot encoding of the targets:
        tags_encoding = [0] * len(self.classes) # all encodings are of same length
        for tag in tags:
            tags_encoding[self.classes.index(tag)] = 1
        
        sample['tags'] = tags_encoding

        return sample

class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __init__(self, image_size):
        assert isinstance(image_size, int) # checks that classes is an int
        self.image_size = image_size

    def __call__(self, sample):
        
        image, title, description, tags = sample['image'], sample['title'], sample['description'], sample['tags']

        # swap color axis because
        # numpy image: H x W x C
        # torch image: C X H X W

        # Change bad items to corrected ones
        if len(image.shape)!=3:
            img = np.zeros((self.image_size,self.image_size,3))
            img[:,:,0] = image
            img[:,:,1] = image
            img[:,:,2] = image
            img = img.transpose((2, 0, 1))
            return {'image': torch.from_numpy(img), #.float added
                    'title': torch.Tensor(title),
                    'description': torch.Tensor(description),
                    'tags': torch.Tensor(tags)}
        else:
            image = image.transpose((2, 0, 1))
            return {'image': torch.from_numpy(image), #.float added
                    'title': torch.Tensor(title),
                    'description': torch.Tensor(description),
                    'tags': torch.Tensor(tags)}

class ImageNetTransform(object):
    """Normalizes the sample image (a tensor) according to the args transform."""

    def __init__(self, image_transform):
        self.image_transform = image_transform

    def __call__(self, sample): 
        sample['image'] = self.image_transform(sample['image'])
        return sample

##########################
## Functions definition ##
##########################

def get_Rakuten_Dataset(dataset_dir, token = "character", encoding="labelencoding", image_size=150):
    """Build the Rakuten Dataset 
        token       str, specify whether a token should correspond to a character or a word
        encoding    str, specify whether title and description encodings should be a CountVectorizer or a Label Encoding
    """

    ## 1/ list of the 19 classes: ##
    classes = ['Beige', 'Black', 'Blue', 'Brown', 'Burgundy', 'Gold', 'Green', 'Grey', 'Khaki', 'Multiple Colors', 
                'Navy', 'Orange', 'Pink', 'Purple', 'Red', 'Silver', 'Transparent', 'White', 'Yellow']

    ## 2/ define the vocabulary for encoding titles and descriptions of both training and test sets:

    print("building the vocabulary")
    token_to_ix = myutils.token_to_ix(dataset_dir,token=token) # vocabulary

    if encoding == "vectorizer":
        print("building the vectorizer")
        vectorizer = myutils.vectorizer(token_to_ix,token=token) # vectorizer
    else:
        vectorizer = None
        
    # 3/ build the transform to be applied on the dataset:
    imagenet_preprocessing = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]) # needed when using pre-trained models

    # transform ImageNetTransform(imagenet_preprocessing) should be added at the end when using pre-trained DenseNet121 CNN:
    trainval_transform = transforms.Compose([Rescale(image_size), VocabEncode(token_to_ix, token, vectorizer), TargetEncode(classes), ToTensor(image_size)])
    test_transform = transforms.Compose([dataset_test.Rescale(image_size), dataset_test.VocabEncode(token_to_ix, token, vectorizer), dataset_test.ToTensor(image_size)])

    # 4/ build the transformed dataset from the source files:
    trainval_dataset = RakutenDataset(dataset_dir=dataset_dir, transform=trainval_transform)
    test_dataset = dataset_test.RakutenDataset(dataset_dir=dataset_dir, transform=test_transform)

    return trainval_dataset, test_dataset

def show_product(image, title, description, tags, show_description=False):
    """Displays a product with its image, title, description and color tags"""
    fig, axarr = plt.subplots(2,1)
    fig.suptitle(title, fontproperties=fprop, wrap=True)
    axarr[0].imshow(image)
    axarr[0].text(0.2, 0.7, tags, fontproperties=fprop, wrap=False, 
                    bbox=dict(facecolor='red', alpha=0.5), transform=plt.gcf().transFigure)
    if show_description :
        axarr[1].text(.5, 0, description, ha='center', va='bottom', fontproperties=fprop, wrap=True)
    plt.axis('off')
    plt.show()

def description_len(description):
    """Tries to return the length of a description, returns the description if not possible"""
    try:
        res=len(description)
    except Exception as e:
        res=description
    return res



if __name__ == "__main__":

    dataset_dir = "/opt/Datasets/RakutenMultiModalColourExtraction/"

    ################################
    ## rakuten data organisation: ##
    ################################

    """
    The columns of the input files (X_train.csv and X_test.csv) are:

    image_file_name - The name of the image file in images folder corresponding to the item.
    item_name - The item title, a short text summarizing the item.
    item_caption - A more detailed text describing the item. (can be either text or NaN value)

    exemple:

    image_file_name,item_name,item_caption
    296409_10002365_1.jpg,【 UES（ウエス） 】 67LW オリジナル吊り編み鹿の子ポロシャツ [ Tシャツ ][ アメカジ ] [ メンズ ] [ 送料・代引き手数料無料 ],【 商品について 】 ※以下の点を予めご了承下さい。 ■実寸サイズは当店在庫の1点を採寸しております。個体差により、若干のサイズ誤差が生じる場合がございます。 ■商品画像はお使いのパソコン・携帯の種類や環境により、色・質感等に若干の誤差が生じる場合がございます。 ■この商品は当店実店舗・他サイトでも販売しております。在庫数の更新は随時行っておりますが、時間差により、品切れになってしまうこともございます。
    214151_10015053_1.jpg,SALE ゴルフウェア メンズ トップス ベスト 春 おしゃれ 大きいサイズ ゴルフ ウェア アーガイル チェック メンズ ニット Vネック スポーツウェア 大人 サックス ピンク M〜XXLアーガイル柄Vネックスプリングゴルフベスト(CG-BS913),---- ギフト用の包袋をご一緒にいかがでしょうか ----▼関連キーワードゴルフウェア メンズ ゴルフ ウェアメンズ ストレッチ おしゃれ 大きいサイズ ゴルフ 飛距離upゴルフウエア ストレッチ 飛距離アップ ゴルフパンツ ストレッチ春 夏 秋 冬 メンズ

    """

    #########################
    ## Manual Data display ##
    #########################

    """
    X_df = pd.read_csv(dataset_dir + '/X_train.csv', encoding='utf-8').iloc[:, 1:] # encoding to utf-8 from the start
    y_df = pd.read_csv(dataset_dir + '/y_train.csv', encoding='utf-8').iloc[:, 1:] # encoding to utf-8 from the start
    df = pd.merge(X_df, y_df, left_index=True, right_index=True)

    print("X_df shape is: {}".format(X_df.shape))
    print("y_df shape is: {}".format(y_df.shape))
    print("merged dataframe df shape is: {}".format(df.shape))
    print(df.head())

    k = 0
    img_name = df.iloc[k, 0]
    img_title = df.iloc[k, 1]
    img_description = df.iloc[k, 2]
    tags = df.iloc[k, 3]

    print('Image name: {}'.format(img_name))
    print('Image title: {}'.format(img_title))
    print('Image description: {}'.format(img_description))
    print('Color tags: {}'.format(tags))

    show_product(io.imread(os.path.join(dataset_dir + "/images", img_name)), img_title, 
                    img_description, tags, show_description=False)
    """

    ##########################################
    ## instanciate the RakutenDataset class ##
    ##########################################

    product_dataset = RakutenDataset(dataset_dir=dataset_dir)

    print("product_dataset has {} enteries".format(len(product_dataset)))

    """
    for i in range(50):
        sample = product_dataset[i]
        print(i, sample['image'].shape, len(sample['title']), description_len(sample['description']), len(sample['tags']))

        if i==2:
            show_product(**sample, show_description=True)
    """

    ########################
    ## dataset statistics ##
    ########################
    """
    In this section we perform:
        -> for the images: we collect the heigts and widths.
        -> for the title and descriptions: we build a vocabulary.
        -> for the targets: we list the different targets.
    """
    
    """
    heights=[]
    widths=[]
    targets={}
    target_comb={} # popular targets combinations

    for e in tqdm(product_dataset):
    #for i in tqdm(range(1000)):
        #e = product_dataset[i]

        image_size=e['image'].shape
        # image_sizes update:
        heights.append(image_size[0])
        widths.append(image_size[1])

        tags=e['tags']

        str_tags=str(tags)
        # target_comb update:
        if str_tags in target_comb:
            target_comb[str_tags]+=1
        else:
            target_comb[str_tags]=1
        
        # targets update:
        for tag in tags:
            if tag in targets:
                targets[tag]+=1
            else:
                targets[tag]=1

    target_pop = dict( (k,v) for k,v in target_comb.items() if 200 <= v)    # popular targets combinations
    target_pop = dict(sorted(target_pop.items(), key=lambda item: item[1], reverse=True)) # (sorted by decreasing values)

    # print("Color frequences are:{}".format(targets))
    # print("Most common color combinations are:{}".format(target_pop))

    textfile_tags = open('/stats/color_tags_stats.txt', 'w')
    textfile_tags.write(str(targets))
    textfile_tags.close()

    textfile_pop_tags = open('/stats/color_tags_pop_comb_stats.txt', 'w')
    textfile_pop_tags.write(str(target_pop))
    textfile_pop_tags.close()

    # plot image dimensions using histogram representation:

    hwidths, bins = np.histogram(widths, bins=20)
    hheights, bins = np.histogram(heights, bins=20)

    fig, ax = plt.subplots(1,2)
    width = 0.7 * (bins[1] - bins[0])
    center = (bins[:-1] + bins[1:]) / 2
    ax[0].bar(center, hwidths, color='blue',align='center', width=width, label="widths")
    ax[1].bar(center, hheights, color='red',align='center', width=width, label="heights")
    handles, labels = [(a + b) for a, b in zip(ax[0].get_legend_handles_labels(), ax[1].get_legend_handles_labels())]
    fig.legend(handles, labels, loc='upper right')
    fig.suptitle("Image dimensions distribution")
    fig.canvas.set_window_title("image_dimensions_distribution")
    plt.show()
    """

    #####################
    ## image transform ##
    #####################

    """
    sample = product_dataset[0]

    print("original image sample dimensions: {}".format(sample['image'].shape))
    show_product(**sample, show_description=True)

    scale = Rescale(200) # create a scale transform
    transformed_sample = scale(sample)

    print("rescaled image sample dimensions: {}".format(sample['image'].shape))
    show_product(**sample, show_description=True)
    """

    #############################################
    ## dataset with the appropriate transforms ##
    #############################################

    token = "character"
    vectorizer = False

    # 1/ building vocabulary for targets and features (title, description)

    # 1.1/ list of the 19 classes: 
    classes = ['Beige', 'Black', 'Blue', 'Brown', 'Burgundy', 'Gold', 'Green', 'Grey', 'Khaki', 'Multiple Colors', 
                'Navy', 'Orange', 'Pink', 'Purple', 'Red', 'Silver', 'Transparent', 'White', 'Yellow']

    # 1.2/ define the vocabulary for encoding titles and descriptions of both training and test sets:

    print("building the vocabulary")
    token_to_ix = myutils.token_to_ix(dataset_dir,token=token) # vocabulary
    
    if vectorizer:
        print("building the vectorizer")
        vectorizer = myutils.vectorizer(token_to_ix,token=token) # vectorizer

    # 2/ build the transforms and apply them on the dataset:

    transform = transforms.Compose([Rescale(300), VocabEncode(token_to_ix, vectorizer=vectorizer), TargetEncode(classes), ToTensor(image_size=300)])
    transformed_dataset = RakutenDataset(dataset_dir=dataset_dir, transform=transform)

    print("transformed_dataset has {} enteries".format(len(transformed_dataset)))

    for i in range(len(transformed_dataset)):
        sample = transformed_dataset[i]

        print(i, sample['image'].size(), sample['title'].size(), sample['description'].size(), sample['tags'].size())
        # print(i, sample['title'], sample['description'])
        # print(sample['tags'])
        
        if i==3:
            break






