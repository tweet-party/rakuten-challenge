# -*- coding: utf-8 -*-

##########################
##   Import Libraries   ##
##########################

from __future__ import print_function, division, unicode_literals
import os, sys, ast
from tqdm import tqdm
import cv2
import re
import string
import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
#plt.ion()   # interactive mode

import matplotlib.font_manager as fm
font_file = "./Varieties/NotoSansCJKtc-Regular.otf"
fprop = fm.FontProperties(fname=font_file)

from skimage import io
from skimage import transform as skimage_transform # to avoid conflict
from skimage.color import rgba2rgb
from PIL import ImageFile
from PIL import Image
ImageFile.LOAD_TRUNCATED_IMAGES = True

import torch
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils

import warnings # Ignore warnings
#warnings.filterwarnings("ignore")

import utils as myutils

###########################################
## Rakuten Test Dataset class definition ##
###########################################

class RakutenDataset(Dataset):
    """Rakuten test dataset."""

    def __init__(self, dataset_dir, transform=None):
        """
        Args:
            dataset_dir (string): Path to the dataset directory.
            transform (callable, optional): Optional transform to be applied on a sample.
        """
        self.dataset_dir = dataset_dir
        self.rakuten_frame = pd.merge(pd.read_csv(self.dataset_dir + '/X_test.csv', encoding='utf-8').iloc[:, 1:], 
                pd.read_csv('Challenge_info/y_random_submission_exemple.csv', encoding='utf-8').iloc[:, 1:], 
                left_index=True, right_index=True)
        self.transform = transform

    def __len__(self):
        return len(self.rakuten_frame)

    def __getitem__(self, idx):

        if torch.is_tensor(idx):
            idx = idx.tolist()

        img_name = os.path.join(self.dataset_dir + "/images/", self.rakuten_frame.iloc[idx, 0])
        image = io.imread(img_name) # ---> one image is corrupted and causes io.imread to fail <---

        title = self.rakuten_frame.iloc[idx, 1]
        description = self.rakuten_frame.iloc[idx, 2]

        sample = {'image': image, 'title': title, 'description': description}

        if self.transform:
            sample = self.transform(sample)

        return sample

#################################
## Transforms class definition ##
#################################

class Rescale(object):
    """Rescales the image in a sample to a given size.

    Args:
        output_size (tuple or int): Desired output size. If tuple, output is
            matched to output_size. If int, output is matched to (int,int) size.
    """
    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple)) #checks that arg output_size is either int or tuple
        self.output_size = output_size

    def __call__(self, sample):

        image = sample['image']

        if isinstance(self.output_size, int):
            # if arg output_size an int:
            new_h, new_w = self.output_size, self.output_size
        else:
            # if arg output_size a tuple:
            new_h, new_w = self.output_size

        img = skimage_transform.resize(image, (new_h, new_w))
        sample['image'] = img

        return sample

class VocabEncode(object):
    """Encodes tile and descriptions according to vocabulary to_idx or a vectorizer"""

    def __init__(self, to_ix, token = "word", vectorizer=None):
        assert isinstance(to_ix, dict) # checks that to_ix is a dictionary
        self.to_ix = to_ix
        self.vectorizer = vectorizer
        self.token = token

    def __call__(self, sample):

        if self.vectorizer:
            # CountVectorizer: usefull to directly perform the multi outputs classification
            sample['title'] = self.vectorizer.transform( [myutils.text_preprocessing(sample['title'])] ).toarray() [0]
            sample['description'] = self.vectorizer.transform( [myutils.text_preprocessing(sample['description'])] ).toarray() [0]

        else:
            # Label encoding: usefull for RNN input
            sample['title'] = myutils.prepare_sequence(myutils.text_preprocessing(sample['title']), self.to_ix, self.token)
            sample['description'] = myutils.prepare_sequence(myutils.text_preprocessing(sample['description']), self.to_ix, self.token)

        return sample

class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __init__(self, image_size):
        assert isinstance(image_size, int) # checks that classes is an int
        self.image_size = image_size

    def __call__(self, sample):
        
        #image, title, description, tags = sample['image'], sample['title'], sample['description'], sample['tags']
        image, title, description = sample['image'], sample['title'], sample['description']

        # swap color axis because
        # numpy image: H x W x C
        # torch image: C X H X W
        # Change bad items to corrected ones
        if len(image.shape)!=3:
            img = np.zeros((self.image_size,self.image_size,3))
            img[:,:,0] = image
            img[:,:,1] = image
            img[:,:,2] = image
            img = img.transpose((2, 0, 1))
            return {'image': torch.from_numpy(img), #.float added
                    'title': torch.Tensor(title),
                    'description': torch.Tensor(description)}
        else:
            image = image.transpose((2, 0, 1))
            return {'image': torch.from_numpy(image), #.float added
                    'title': torch.Tensor(title),
                    'description': torch.Tensor(description)}

class ImageNetTransform(object):
    """Normalizes the sample image (a tensor) according to the args transform."""

    def __init__(self, image_transform):
        self.image_transform = image_transform

    def __call__(self, sample):
        
        sample['image'] = self.image_transform(sample['image'])

        return sample


if __name__ == "__main__":

    ################
    ## parameters ##
    ################

    dataset_dir = "/opt/Datasets/RakutenMultiModalColourExtraction/"
    token = "character"
    vectorizer = False

    product_dataset = RakutenDataset(dataset_dir=dataset_dir)
    print("product_dataset has {} enteries".format(len(product_dataset)))

    # 1/ building vocabulary for the features (title, description) 

    print("building the vocabulary")
    token_to_ix = myutils.token_to_ix(dataset_dir,token=token) # vocabulary
    
    if vectorizer:
        print("building the vectorizer")
        vectorizer = myutils.vectorizer(token_to_ix,token=token) # vectorizer

    # 2/ build the transforms and apply them on the dataset:

    transform = transforms.Compose([Rescale(150), VocabEncode(token_to_ix, vectorizer=vectorizer), ToTensor(image_size=150)])
    transformed_dataset = RakutenDataset(dataset_dir=dataset_dir, transform=transform)

    print("transformed_dataset has {} enteries".format(len(transformed_dataset)))

    for i in range(len(transformed_dataset)):
        sample = transformed_dataset[i]

        print(i, sample['image'].size(), sample['title'].size(), sample['description'].size())
        # print(i, sample['title'], sample['description'])
        
        if i==3:
            break














