# -*- coding: utf-8 -*-

##########################
##   Import Libraries   ##
##########################

from numpy import random
import numpy as np, os, sys, re

import torch
import torch.nn as nn
from torch import optim
import torchvision 
import torchvision.transforms as transforms
from torch.nn.modules.module import _addindent

import functools
import operator

#######################
## Elementary Layers ##
#######################

def linear(dim_in, dim_out):
    return [nn.Linear(dim_in, dim_out)]

def linear_relu(dim_in, dim_out):
    return [nn.Linear(dim_in, dim_out),
            nn.ReLU(inplace=True)]

def dropout_linear(dim_in, dim_out, p_drop):
    return [nn.Dropout(p_drop),
            nn.Linear(dim_in, dim_out)]

def dropout_linear_relu(dim_in, dim_out, p_drop):
    return [nn.Dropout(p_drop),
            nn.Linear(dim_in, dim_out),
            nn.ReLU(inplace=True)]

def bn_dropout_linear(dim_in, dim_out, p_drop):
    return [nn.BatchNorm1d(dim_in),
            nn.Dropout(p_drop),
            nn.Linear(dim_in, dim_out)]

def bn_linear_relu(dim_in, dim_out):
    return [nn.BatchNorm1d(dim_in),
            nn.Linear(dim_in, dim_out),
            nn.ReLU(inplace=True)]

def bn_dropout_linear_relu(dim_in, dim_out, p_drop):
    return bn_dropout_linear(dim_in, dim_out, p_drop) + [nn.ReLU(inplace=True)]
            
def conv_relu_maxpool(cin, cout, csize, cstride, cpad, msize, mstride, mpad):
    return [nn.Conv2d(cin, cout, csize, cstride, cpad),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(msize, mstride, mpad)]

def conv_bn_relu_maxpool(cin, cout, csize, cstride, cpad, msize, mstride, mpad):
    return [nn.Conv2d(cin, cout, csize, cstride, cpad),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(cout),
            nn.MaxPool2d(msize, mstride, mpad)]    

def lstm(input_size, dim_hidden, num_layers):
    return [nn.LSTM(input_size=input_size, 
                    hidden_size=dim_hidden, 
                    num_layers=num_layers,
                    batch_first=True,
                    bidirectional=True)]    

#################################
## Our DenseNet121 + Rnn Model ##
#################################

class Conv121RnnNet(nn.Module):
    def __init__(self, vocab_size = 3766, image_size = 150, embedding_size = 10, dim_hidden = 10, num_layers = 5, p_drop = 0.6):
        super(Conv121RnnNet, self).__init__()

        self.vocab_size = vocab_size
        self.num_classes = 19     
        self.image_size = image_size 
        self.embedding_size = embedding_size
        self.dim_hidden = dim_hidden
        self.num_layers = num_layers
        self.p_drop = p_drop

        self.empty_tensor_img = torch.randn(1, 3, self.image_size, self.image_size)
        self.empty_tensor_text = torch.zeros(1, 3766)

        self.model_dens = torchvision.models.densenet121(pretrained=True)
        self.conv_layer= nn.Sequential(*list(self.model_dens.children())[:-2])        

        self.embedding = nn.Embedding(self.vocab_size, self.embedding_size)
        self.lstm_layer = nn.Sequential(
            *lstm(input_size = self.embedding_size,
                    dim_hidden = self.dim_hidden, 
                    num_layers = self.num_layers))

        self.output_size_img = self.conv_layer(self.empty_tensor_img).shape[2]*self.conv_layer(self.empty_tensor_img).shape[2]*self.conv_layer(self.empty_tensor_img).shape[1]

        self.fc_model = nn.Sequential(
            *bn_dropout_linear_relu(self.output_size_img + 2*2*self.dim_hidden, 1024, self.p_drop), # many to one
            *bn_dropout_linear_relu(1024, 512, self.p_drop),            
            *bn_dropout_linear_relu(512, 256, self.p_drop),
            nn.Linear(256, self.num_classes))

    def forward(self, inputs):

        # 1/ CNN forward on the image features:
        conv_features = self.conv_layer(inputs[0])
        conv_features = conv_features.view(conv_features.size(0), -1)

        # 2/ LSTM forward on the title features:
        #print("tensor title à l'entrée du lstm: {}".format(inputs[1].size()))
        lstm_features_title = self.embedding(inputs[1])
        #print("tensor title après embedding: {}".format(lstm_features_title.size()))
        lstm_features_title, _ = self.lstm_layer(lstm_features_title)
        #print("tensor title à la sortie du lstm: {}".format(lstm_features_title.size()))
        lstm_features_title = lstm_features_title[:,-1,:] # many to one lstm:
        #print("tensor title à la sortie du lstm, last sequence element: {}".format(lstm_features_title.size()))
        lstm_features_title = lstm_features_title.reshape(lstm_features_title.size(0), -1)

        # 3/ LSTM forward on the description features:
        lstm_features_descrip = self.embedding(inputs[2])
        lstm_features_descrip, __ = self.lstm_layer(lstm_features_descrip)
        lstm_features_descrip = lstm_features_descrip[:,-1,:] # many to one lstm
        lstm_features_descrip = lstm_features_descrip.reshape(lstm_features_descrip.size(0), -1)
        #print("conv_features: {}".format(conv_features.size()))
        #print("lstm_features_title: {}".format(lstm_features_title.size()))
        #print("lstm_features_description: {}".format(lstm_features_descrip.size()))
        
        # Note: many to many require padding over the dataset before the fully connected layers:
        
        # 4/ combinaison of the three different intermidiate ouputs: 
        features_combined = torch.cat([conv_features,lstm_features_title,lstm_features_descrip], 1)
        
        # 5/ fully connected layer classification:
        out = self.fc_model(features_combined)

        return out

#######################
## Our CNN RNN model ##
#######################

class ConvRnnNet(nn.Module):
    def __init__(self, vocab_size = 3766, image_size = 150, embedding_size = 10, dim_hidden = 10, num_layers = 5, p_drop = 0.6):
        super(ConvRnnNet, self).__init__()

        self.vocab_size = vocab_size
        self.num_classes = 19 
        self.image_size = image_size     
        self.embedding_size = embedding_size
        self.dim_hidden = dim_hidden
        self.num_layers = num_layers
        self.p_drop = p_drop

        self.empty_tensor_img = torch.randn(1, 3, self.image_size, self.image_size)
        self.empty_tensor_text = torch.zeros(1, 3766)

        self.conv_layer = nn.Sequential(
            *conv_bn_relu_maxpool(cin=3, cout=32,
                                csize=5, cstride=2, cpad=3,
                                msize=2, mstride=1, mpad=0),
            *conv_bn_relu_maxpool(cin=32, cout=64,
                                csize=5, cstride=2, cpad=3,
                                msize=2, mstride=1, mpad=0),
            *conv_bn_relu_maxpool(cin=64, cout=128,
                                csize=3, cstride=1, cpad=0,
                                msize=2, mstride=1, mpad=0),
            *conv_bn_relu_maxpool(cin=128, cout=128,
                                csize=3, cstride=1, cpad=0,
                                msize=2, mstride=1, mpad=0))     

        self.embedding = nn.Embedding(self.vocab_size, self.embedding_size)
        self.lstm_layer = nn.Sequential(
            *lstm(input_size = self.embedding_size,
                    dim_hidden = self.dim_hidden, 
                    num_layers = self.num_layers))

        self.output_size_img = self.conv_layer(self.empty_tensor_img).shape[2]*self.conv_layer(self.empty_tensor_img).shape[2]*self.conv_layer(self.empty_tensor_img).shape[1]

        self.fc_model = nn.Sequential(
            *bn_dropout_linear_relu(self.output_size_img + 2*2*self.dim_hidden, 512, self.p_drop), # many to one
            *bn_dropout_linear_relu(512, 256, self.p_drop),            
            *bn_dropout_linear_relu(256, 64, self.p_drop),
            nn.Linear(64, self.num_classes))

    def forward(self, inputs):

        # 1/ CNN forward on the image features:
        conv_features = self.conv_layer(inputs[0])
        conv_features = conv_features.view(conv_features.size(0), -1)

        # 2/ LSTM forward on the title features:
        #print("tensor title à l'entrée du lstm: {}".format(inputs[1].size()))
        lstm_features_title = self.embedding(inputs[1])
        #print("tensor title après embedding: {}".format(lstm_features_title.size()))
        lstm_features_title, _ = self.lstm_layer(lstm_features_title)
        #print("tensor title à la sortie du lstm: {}".format(lstm_features_title.size()))
        lstm_features_title = lstm_features_title[:,-1,:] # many to one lstm:
        #print("tensor title à la sortie du lstm, last sequence element: {}".format(lstm_features_title.size()))
        lstm_features_title = lstm_features_title.reshape(lstm_features_title.size(0), -1)

        # 3/ LSTM forward on the description features:
        lstm_features_descrip = self.embedding(inputs[2])
        lstm_features_descrip, __ = self.lstm_layer(lstm_features_descrip)
        lstm_features_descrip = lstm_features_descrip[:,-1,:] # many to one lstm
        lstm_features_descrip = lstm_features_descrip.reshape(lstm_features_descrip.size(0), -1)
        #print("conv_features: {}".format(conv_features.size()))
        #print("lstm_features_title: {}".format(lstm_features_title.size()))
        #print("lstm_features_description: {}".format(lstm_features_descrip.size()))
        
        # Note: many to many require padding over the dataset before the fully connected layers:
        
        # 4/ combinaison of the three different intermidiate ouputs: 
        features_combined = torch.cat([conv_features,lstm_features_title,lstm_features_descrip], 1)
        
        # 5/ fully connected layer classification:
        out = self.fc_model(features_combined)

        return out

###################################
## Our ConvCountVectorizer Model ##
###################################

class ConvCountVectorizer(nn.Module):
    def __init__(self, vocab_size = 3766, image_size = 100):
        super(ConvCountVectorizer, self).__init__()

        self.num_classes = 19
        self.vocab_size = vocab_size 
        self.image_size = image_size       

        self.empty_tensor_img = torch.randn(1, 3, self.image_size, self.image_size)

        self.conv_layer = nn.Sequential(
            *conv_bn_relu_maxpool(cin=3, cout=32,
                                csize=5, cstride=2, cpad=3,
                                msize=2, mstride=1, mpad=0),
            *conv_bn_relu_maxpool(cin=32, cout=64,
                                csize=5, cstride=2, cpad=3,
                                msize=2, mstride=1, mpad=0),
            *conv_bn_relu_maxpool(cin=64, cout=128,
                                csize=3, cstride=1, cpad=0,
                                msize=2, mstride=1, mpad=0),
            *conv_bn_relu_maxpool(cin=128, cout=128,
                                csize=3, cstride=1, cpad=0,
                                msize=2, mstride=1, mpad=0))

        self.output_size_img = self.conv_layer(self.empty_tensor_img).shape[2]*self.conv_layer(self.empty_tensor_img).shape[2]*self.conv_layer(self.empty_tensor_img).shape[1]

        self.fc_model = nn.Sequential(
            *bn_dropout_linear_relu(self.output_size_img + 2*self.vocab_size, 1024, 0.5),
            *bn_dropout_linear_relu(1024, 512, 0.5),
            *bn_dropout_linear_relu(512, 256, 0.5),
            nn.Linear(256, self.num_classes))

    def forward(self, inputs):

        # 1/ CNN forward on the image features:
        conv_features = self.conv_layer(inputs[0])
        conv_features = conv_features.view(conv_features.size(0), -1)

        # 2/ title and description CountVectorizer outputs:
        features_title = inputs[1]
        features_descrip = inputs[2]
        
        # 3/ combinaison of the CNN output with the CountVectorizer output: 
        features_combined = torch.cat([conv_features,features_title,features_descrip], 1)

        # 4/ fully connected layer classification
        out = self.fc_model(features_combined)

        return out


###############################################################
## Pretrained CNN Densenet121 applied on image features only ##
###############################################################

class Conv121Net(nn.Module):
    def __init__(self,image_size=100):
        super(Conv121Net, self).__init__()

        self.num_classes = 19
        self.image_size = image_size
        self.empty_tensor = torch.randn(1, 3, self.image_size, self.image_size)
        self.model_dens = torchvision.models.densenet121(pretrained=True)
        self.conv_layer= nn.Sequential(*list(self.model_dens.children())[:-2])

        self.output_size =self.conv_layer(self.empty_tensor).shape[2]*self.conv_layer(self.empty_tensor).shape[2]*self.conv_layer(self.empty_tensor).shape[1]
        self.fc_model = nn.Sequential(
            *bn_dropout_linear_relu(self.output_size, 1024, 0.5),
            *bn_dropout_linear_relu(1024, 512, 0.5),
            *bn_dropout_linear_relu(512, 256, 0.5),
            nn.Linear(256, self.num_classes))

    def forward(self, inputs):
        conv_features = self.conv_layer(inputs[0])
        conv_features = conv_features.view(conv_features.size(0), -1)
        return self.fc_model(conv_features)


############################################
## Our CNN applied on image features only ##
############################################

class MyConvNet(nn.Module):

    def __init__(self,image_size=150):
        super(MyConvNet, self).__init__()

        self.num_classes = 19
        self.image_size = image_size
        self.empty_tensor = torch.randn(1, 3, self.image_size, self.image_size)
        self.conv_layer = nn.Sequential(
            *conv_bn_relu_maxpool(cin=3, cout=32,
                                csize=5, cstride=2, cpad=3,
                                msize=2, mstride=1, mpad=0),
            *conv_bn_relu_maxpool(cin=32, cout=64,
                                csize=5, cstride=2, cpad=3,
                                msize=2, mstride=1, mpad=0),
            *conv_bn_relu_maxpool(cin=64, cout=128,
                                csize=3, cstride=1, cpad=0,
                                msize=2, mstride=1, mpad=0),
            *conv_bn_relu_maxpool(cin=128, cout=128,
                                csize=3, cstride=1, cpad=0,
                                msize=2, mstride=1, mpad=0))

        self.output_size =self.conv_layer(self.empty_tensor).shape[2]*self.conv_layer(self.empty_tensor).shape[2]*self.conv_layer(self.empty_tensor).shape[1]
        
        self.fc_model = nn.Sequential(
            *bn_dropout_linear_relu(self.output_size, 1024, 0.5),
            *bn_dropout_linear_relu(1024, 512, 0.5),
            *bn_dropout_linear_relu(512, 256, 0.5),
            nn.Linear(256, self.num_classes))
        
    def forward(self, inputs):
        conv_features = self.conv_layer(inputs[0])
        conv_features = conv_features.view(conv_features.size(0), -1)
        return self.fc_model(conv_features)

