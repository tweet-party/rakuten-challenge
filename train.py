# -*- coding: utf-8 -*-

##########################
##   Import Libraries   ##
##########################

from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

import torch
import torch.nn as nn
from torch.utils.data import DataLoader, sampler
from torch.utils.tensorboard import SummaryWriter
from  torch.optim.lr_scheduler import StepLR

import pandas as pd
import numpy as np
import os
import argparse
from datetime import datetime

import utils
import dataset
from models import Conv121RnnNet, ConvRnnNet, ConvCountVectorizer, Conv121Net, MyConvNet

start_time = datetime.now()

####################
## Parser Section ##
####################

parser = argparse.ArgumentParser(formatter_class = argparse.RawTextHelpFormatter)
parser.add_argument('--model', type=str,
                    help="The model name (a str). \n"
                    " e.g.: ConvRnnNet, ConvCountVectorizer or Conv121Net",
                    required=True)
parser.add_argument('--token', type=str,
                    help="Token type (a str). \n"
                    " Can either be 'character' or 'word'.",
                    required=False,
                    default="character")
parser.add_argument('--image_size', type=int,
                    help="Desired image size (an int). \n"
                    " e.g.: 100, 150 or 300.",
                    required=False,
                    default=150)
args = parser.parse_args()

################
## Parameters ##
################

dataset_dir = "/opt/Datasets/RakutenMultiModalColourExtraction/"

classes = ['Beige', 'Black', 'Blue', 'Brown', 'Burgundy', 'Gold', 'Green', 'Grey', 'Khaki', 'Multiple Colors', 
            'Navy', 'Orange', 'Pink', 'Purple', 'Red', 'Silver', 'Transparent', 'White', 'Yellow']

#percentage_classes not well calculated yet .. this is why I commented it
'''
percentage_classes = [0.1210, 0.3386, 0.1604, 0.1572, 0.0112, 0.0391, 0.1023, 0.1358, 0.0394,
            0.0594, 0.1179, 0.0489, 0.1159, 0.0488, 0.1112, 0.0609, 0.0157, 0.2440, 0.0685]
print(sum(percentage_classes))
pos_weight = torch.FloatTensor([((1-x)/x) for x in percentage_classes])
print(pos_weight)
'''

model_name = args.model
device = torch.device('cuda')
validation_split = .3
shuffle_dataset = True
num_workers = 20
num_epochs = 30
batch_size = 32
learning_rate = 0.01
num_classes = len(classes)
image_size = args.image_size
token = args.token
if model_name == "ConvCountVectorizer":
    encoding = "vectorizer"
    collate_fn = utils.vectorizer_collate_fonction(image_size=image_size)
else:
    encoding = "labelencoding"
    collate_fn = utils.custom_collate_fonction(image_size=image_size)

###################################################
## Train, valid  and test Data loader definition ##
###################################################

trainval_dataset, test_dataset = dataset.get_Rakuten_Dataset(dataset_dir=dataset_dir, token=token, encoding=encoding, image_size=image_size)

trainval_size = len(trainval_dataset)
test_size = len(test_dataset)

print("The Rakuten trainval_dataset has {} enteries.".format(trainval_size)) 
print("The Rakuten test_dataset has {} enteries.".format(test_size))

indices = list(range(trainval_size)) # Creating data indices for training and validation splits
split = int(np.floor(validation_split * trainval_size))

if shuffle_dataset :
    np.random.seed(42) # random seeds = 42
    np.random.shuffle(indices)
train_indices, val_indices = indices[split:], indices[:split]

# Creating PT data samplers:
train_sampler = sampler.SubsetRandomSampler(train_indices)
valid_sampler = sampler.SubsetRandomSampler(val_indices)

# Creating data loaders:
train_loader = DataLoader(trainval_dataset, batch_size=batch_size, sampler=train_sampler, num_workers=num_workers, collate_fn=collate_fn)
valid_loader = DataLoader(trainval_dataset, batch_size=batch_size, sampler=valid_sampler, num_workers=num_workers, collate_fn=collate_fn)
test_loader = DataLoader(test_dataset, batch_size=batch_size, num_workers=num_workers, collate_fn=collate_fn)

print("The Rakuten train_loader has {} batches.".format(len(train_loader)))
print("The Rakuten valid_loader has {} batches.".format(len(valid_loader)))
print("The Rakuten test_loader has {} batches.".format(len(test_loader)))

##########################
## Model Initialisation ##
##########################

if model_name == "ConvRnnNet":
    model = ConvRnnNet(image_size=image_size)
    model_test = ConvRnnNet(image_size=image_size)
elif model_name == "Conv121RnnNet":
    model = Conv121RnnNet(image_size=image_size)
    model_test = Conv121RnnNet(image_size=image_size)
elif model_name == "ConvCountVectorizer":
    model = ConvCountVectorizer(image_size=image_size)
    model_test = ConvCountVectorizer(image_size=image_size)
elif model_name == "Conv121Net":
    model = Conv121Net(image_size=image_size)
    model_test = Conv121Net(image_size=image_size)
elif model_name == "MyConvNet":
    model = MyConvNet(image_size=image_size)
    model_test = MyConvNet(image_size=image_size)
else:
    raise Exception('Model name given as argument does not correspond to an existing model')

model.to(device)

# Loss and optimizer
#weights = torch.FloatTensor(percentage_classes)
#criterion = nn.BCEWithLogitsLoss(pos_weight=pos_weight)
criterion = nn.BCEWithLogitsLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
scheduler = StepLR(optimizer, step_size=5, gamma=0.1)

################################
## Tensorboard Initialization ##
################################

top_logdir = "./logs"
if not os.path.exists(top_logdir):
    os.mkdir(top_logdir)

logdir = utils.generate_unique_logpath(top_logdir, "Rakuten_" + model_name)
print("Logging to {}".format(logdir))
if not os.path.exists(logdir):
    os.mkdir(logdir)

# Where to save the logs of the metrics:
history_file = open(logdir + '/history', 'w', 1)
history_file.write("Epoch\tTrain loss\tTrain acc\tTrain f1\tVal loss\tVal acc\tVal f1\tTest loss\tTest acc\tTest f1\n")

# Generate and dump the summary of the model:
summary_text = utils.model_summary(model, optimizer, logdir=logdir)

# Initialise tensorboard SummaryWriter:
writer = SummaryWriter(log_dir=logdir)
writer.add_text("Experiment summary", summary_text)

model_path = "./best_" + model_name + ".pt"
model_checkpoint = utils.ModelCheckpoint(model_path, {'model': model})

####################
## Model Training ##
####################

print('Training model...')
for epoch in range(num_epochs):
    
    model.train()
    train_loss, train_acc,train_f1 = utils.train(model, train_loader, criterion, optimizer, device)
    print(f'Training : Epoch [{epoch+1}/{num_epochs}], Loss [{train_loss:.4f}], Accuracy [{train_acc:.4f}], F1_score [{train_f1:.4f}]')

    model.eval()
    val_loss, val_acc,val_f1 = utils.valid(model, valid_loader, criterion, device)
    print(f'Validation : Epoch [{epoch+1}/{num_epochs}], Loss [{val_loss:.4f}], Accuracy [{val_acc:.4f}], F1_score [{val_f1:.4f}]')

    scheduler.step()
    print('Epoch-{0} lr: {1}'.format(epoch+1, optimizer.param_groups[0]['lr']))

    test_loss, test_acc, test_f1 = 0, 0, 0
    history_file.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(epoch,
                                                                 train_loss, train_acc, train_f1,
                                                                 val_loss, val_acc, val_f1,
                                                                 test_loss, test_acc, test_f1))

    model_checkpoint.update(val_loss)
    writer.add_scalar('training_loss', train_loss, epoch)
    writer.add_scalar('training_accuracy', train_acc, epoch)
    writer.add_scalar('training_F1', train_f1 , epoch) 
    writer.add_scalar('validation_loss', val_loss, epoch)
    writer.add_scalar('validation_accuracy', val_acc, epoch)
    writer.add_scalar('validation_F1', val_f1 , epoch)

###########################
## Inference on test set ##
###########################
model_test.to(device)
model_test_dict = torch.load(model_path)
model_test = model_test_dict['model']
model_test.eval()

test_predictions = utils.test(model_test, test_loader, criterion, device, classes)

###############################################
## Save results and generate submission file ##
###############################################

df_preds = pd.read_csv('Challenge_info/y_random_submission_exemple.csv', encoding='utf-8', sep=',')
df_preds['color_tags'] = test_predictions
df_submission = df_preds
df_submission.to_csv('Submission_tags_' + model_name + '.csv', encoding='utf-8', index=False)

end_time = datetime.now()
print('Time of execution of train.py is : {}'.format(end_time - start_time))

